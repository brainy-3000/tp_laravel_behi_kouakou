<?php

namespace Database\Factories;

use App\Models\pay;
use Illuminate\Database\Eloquent\Factories\Factory;

class PayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = pay::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [


                'libelle' => $this->faker->country(),
                'description' => $this->faker->sentence(),
                'code_indicatif' => $this->faker->unique()->countryCode(),
                'continent' => $this->faker->randomElement(['Afrique', 'Europe', 'Amerique', 'Oceanie', 'Antartique']),
                'population' => $this->faker->numberBetween(),
                'capitale' => $this->faker->city(),
                'monnaie' =>  $this->faker->randomElement(['XOF', 'EUR', 'DOLLAR']),
                'langue' =>  $this->faker->randomElement(['FR','EN','AR','ES']),
                'superficie' => $this->faker->numberBetween(),
                'Est_laique' => $this->faker->boolean(),


            //
        ];
    }
}
